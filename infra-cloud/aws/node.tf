terraform {
 
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-west-2"
  default_tags {
    tags = {
      App = "nodejs"
    }
  }
}

resource "aws_instance" "node-js" {
  ami             = "ami-0892d3c7ee96c0bf7" # Ubuntu 20.04 LTS // us-east-1
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.allow_node_ports.name]
  key_name = "nodeKey"
}

data "aws_vpc" "default_vpc" {
  default = true
}

data "aws_subnet_ids" "default_subnet" {
  vpc_id = data.aws_vpc.default_vpc.id
}

resource "aws_security_group" "allow_node_ports" {
    name = "security-group.node"
    description = "open ports for node functionality"

    ingress{
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress{
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress{
        from_port = 3000
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress{
        from_port = 3001
        to_port = 3001
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress{
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "nodeKey" {
  key_name   = "nodeKey"      
  public_key = tls_private_key.pk.public_key_openssh

  provisioner "local-exec" { 
    command = "rm -rf ~/.ssh/nodeKey.pem && echo '${tls_private_key.pk.private_key_pem}' > ~/.ssh/nodeKey.pem && chmod 400 ~/.ssh/nodeKey.pem"
  }
}

output "ec2_public_dns" {
  value = ["${aws_instance.node-js.*.public_dns}"]
}